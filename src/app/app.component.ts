import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent implements OnInit {
  resp = Array<Object>();

  constructor(private data: DataService) { }

  sendCommand() {
    this.data.sendCommand('ping').then(resp => {
      console.log('Component received server response from service.');
      this.resp.push(resp);
    }, error => {
      console.log('Error error! Error in component!');
    });
  }

  ngOnInit() {
    this.data.init();
  }

}
