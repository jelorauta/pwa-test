import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private id = 0;
  private sw: ServiceWorkerContainer;


  constructor() { }

  init() {
    if ('serviceWorker' in navigator) {
      this.sw = navigator.serviceWorker;
    }
  }

  sendCommand(cmd: string) {
    const completeCommand = { command: cmd, id: this.id };
    this.id++;
    if ('serviceWorker' in navigator) {
      console.log('Passing command to service');
      return this.postMessageToSW(completeCommand);
      /*.then(
        (data) => {
          console.log('Passing response to component from service');
          resolve(data);
        },
        (error) => {
          console.log('We have a problem...', error);
          reject(error);
        });*/
    } else {
      return null;
    }
  }

  postMessageToSW(msg: any) {
    return new Promise((resolve, reject) => {
      const msgChannel = new MessageChannel();

      msgChannel.port1.onmessage = (event) => {
        console.log('Service received server response from worker. Passing it down to component.');
        if (event.data.error) {
          reject(event.data);
        } else {
          resolve(event.data);
        }
      };
      this.sw.controller.postMessage(msg, [msgChannel.port2]);
    });

  }
}
