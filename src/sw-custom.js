var socket;

const serverUrl = 'http://192.168.0.101:3000'
socket = io.connect(serverUrl, { transports: ['websocket'] });

var requests = {};

socket.addEventListener("res", response =>{
  console.log("Server responded. Passing data to service.")
  requests[response.ack].postMessage(response)
})

self.addEventListener('message', (event) => {
  console.log("Sending command to server")
  //event.ports[0].postMessage("Hello there! I'm SW responding here!")
  requests[event.data.id] = event.ports[0]
  socket.emit('rpc', event.data);
});
