var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var req = require('request')

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    console.log("Client connected!")
    socket.on('rpc', command => {
        console.log('Received a command! Command: ', command)
        socket.emit("res", handleCommand(command));
    });
});


http.listen(3000, function () {
    console.log('listening on *:3000');
});

function handleCommand(request) {
    var response = { ack: request.id }

console.log(request)

    /*switch (request.command) {
        case "ping": { response.res = "Pong"; break }
        case "color": { response.res = getRandomColor() ; break}
        case "number": { response.res =  Math.floor(Math.random() * 100) ; break}
    }
*/

    if (request.id % 2 == 0 || request.id == 0)
        response.res = "Ping"
    else
        response.res = "Pong"

    return response
}

function getRandomColor() {
    var hex = '0123456789ABCDEF'
    var color = '#'
    for (var i = 0; i < 6; i++) {
        color += hex[Math.floor(Math.random() * 16)]
    }
    return color
}
