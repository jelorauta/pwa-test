# Pwa test

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.5.

## Prerequisites
- Angular CLI
- Node.js
- http-server `npm install http-server -g`

## How to run
1. Spin up the server in `./server` with `node index.js`
2. Change the `serverUrl` variable to correspond with your server address in `sw-custom.js`
3. Build the Angular app `ng build --prod`
4. Go to `./dist/pwa` and run `http-server`
5. App is now running at http://localhost:8080

## Important files
1. Custom ServiceWorker code - [sw-custom.js](https://bitbucket.org/jelorauta/pwa-test/src/master/src/sw-custom.js)
2. Service that calls our SW with .postMessage() - [data.service.js](https://bitbucket.org/jelorauta/pwa-test/src/master/src/app/data.service.ts)
3. The only component we have in this project - [app.componen.ts](https://bitbucket.org/jelorauta/pwa-test/src/master/src/app/app.component.ts)

